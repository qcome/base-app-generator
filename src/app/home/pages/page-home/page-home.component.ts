import { Component, OnInit } from '@angular/core';
import { ConfirmOptionsI } from 'src/app/core/interfaces/confirm-options-i';
import { ModalService } from 'src/app/core/services/modal.service';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.css']
})
export class PageHomeComponent implements OnInit {

  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
    console.log("ngOnInit() PageHomeComponent");
  }

  openModale(): void {
    this.modalService.confirm(<ConfirmOptionsI>{title: "Mon titre", message: "Mon message"}).subscribe(() => console.log("Validé !"));
  }

}
