import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';



@NgModule({
  // declarations: [NavInlineComponent, BtnComponent, TableLightComponent, TableDarkComponent],
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    // SharedModule,
    // TextModule
  ],
  // exports: [NavInlineComponent, BtnComponent, TableLightComponent, TableDarkComponent]
  exports: []
})
export class LibraryModule { }
