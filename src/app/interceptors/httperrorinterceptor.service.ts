import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError, throwError, tap } from "rxjs";
import { AlertOptionsI } from "../core/interfaces/alert-options-i";
import { ModalService } from "../core/services/modal.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private modalService: ModalService){}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        tap(() => console.log("TAP")),
        catchError((error: HttpErrorResponse) => {
          let errorMsg = '';
          if (error.error instanceof ErrorEvent) {
            console.log('this is client side error');
            errorMsg = `Error: ${error.error.message}`;
          }
          else {
            console.log('this is server side error');
            errorMsg = `Error Code: ${error.status}, Message: ${error.message}`;
            this.modalService.error(<AlertOptionsI>{title: "ERREUR HTTP", message: `erreur ${error.status}`})
          }
          console.log(errorMsg);
          return throwError(errorMsg);
        })
      )
  }
}
