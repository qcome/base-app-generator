import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Test } from 'src/app/shared/models/test';
import { TestService } from '../../services/test.service';

@Component({
  selector: 'app-page-test',
  templateUrl: './page-test.component.html',
  styleUrls: ['./page-test.component.css']
})
export class PageTestComponent implements OnInit {
  public collection$!: Observable<Test[]>;

  constructor(
    private testService: TestService
  ) { }

  ngOnInit(): void {
    console.log("ngOnInit() PageTestComponent");
    this.collection$ = this.testService.collection;
    console.log(this.collection$);
  }

}
