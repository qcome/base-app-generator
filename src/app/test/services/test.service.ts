import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Test } from 'src/app/shared/models/test';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  private pCollection!: Observable<Test[]>;
  private urlApi = environment.urlApi;
  constructor(private http: HttpClient) {
    // const error = new HttpErrorResponse({ status: 422 });
    // this.collection = of(error) as any;
    this.collection = this.http.get<Test[]>(`${this.urlApi}tests`);
  }

  //get collection
  get collection(): Observable<Test[]> {
    return this.pCollection;
  }

  //set collection
  set collection(col: Observable<Test[]>) {
    this.pCollection = col;
  }
}
