import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageTestComponent } from './pages/page-test/page-test.component';
import { TestRoutingModule } from './test-routing.module';


@NgModule({
  declarations: [PageTestComponent],
  imports: [
    CommonModule,
    TestRoutingModule
  ]
})
export class TestModule { }
