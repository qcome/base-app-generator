export interface AlertOptionsI {
  message: string;
  title?: string;
}
