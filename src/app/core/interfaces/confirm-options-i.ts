export interface ConfirmOptionsI {
  message: string;
  title?: string;
  errorOnClose?: boolean;
}
