import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LibraryModule } from '../library/library.module';
import { ModalConfirmationComponent } from './components/modal-confirmation/modal-confirmation.component';
import { ModalService } from './services/modal.service';
import { RouterModule } from '@angular/router';
import { ModalAlertComponent } from './components/modal-alert/modal-alert.component';



@NgModule({
  declarations: [HeaderComponent, FooterComponent, ModalAlertComponent],
  imports: [
    CommonModule,
    // TextModule,
    LibraryModule,
    RouterModule,
  ],
  exports: [HeaderComponent, FooterComponent],
  providers: [ModalService],
  entryComponents: []
})
export class CoreModule { }
