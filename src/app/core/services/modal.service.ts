import { Injectable, Input } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Observable, from, catchError, throwError, EMPTY, of } from 'rxjs';
import { ModalAlertComponent } from '../components/modal-alert/modal-alert.component';
import { ModalConfirmationComponent } from '../components/modal-confirmation/modal-confirmation.component';
import { AlertOptionsI } from '../interfaces/alert-options-i';
import { ConfirmOptionsI } from '../interfaces/confirm-options-i';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private ngbModalService: NgbModal) { }

  // confirm(title: string, message: string) {
  //   const modalRef = this.modalService.open(ModalConfirmationComponent);
  //   modalRef.componentInstance.title = title;
  //   modalRef.componentInstance.message = message;
  //   return modalRef.result;
  // }

  confirm(options: ConfirmOptionsI): Observable<void> {
    const modalOptions = <NgbModalOptions> {
      animation: true,
      backdrop : 'static',
      keyboard : false
    };
    const modalRef = this.ngbModalService.open(ModalConfirmationComponent, modalOptions);
    modalRef.componentInstance.title = options.title ?? 'Confirmation';
    modalRef.componentInstance.message = options.message;
    modalRef.componentInstance.changeRef.markForCheck();
    return from(modalRef.result).pipe(
      catchError(err => (options.errorOnClose ? throwError(() => err ?? 'error not handled') : EMPTY))
    );
  }

  info(options: AlertOptionsI): Observable<void> {
    return this.alert(options, false);
  }

  error(options: AlertOptionsI): Observable<void> {
    return this.alert(options, true);
  }

  private alert(options: AlertOptionsI, isError: boolean): Observable<void> {
    const modalOptions = <NgbModalOptions> {
      animation: true,
    };
    const modalRef = this.ngbModalService.open(ModalAlertComponent, modalOptions);
    modalRef.componentInstance.title = options.title ?? 'Alert';
    modalRef.componentInstance.message = options.message;
    modalRef.componentInstance.isError = isError;
    modalRef.componentInstance.changeRef.markForCheck();
    return from(modalRef.result).pipe(
      catchError(() => of(void 0)) // for dismiss
    )

  }
}
