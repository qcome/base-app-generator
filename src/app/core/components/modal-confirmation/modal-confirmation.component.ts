import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-confirmation',
  templateUrl: './modal-confirmation.component.html',
  styleUrls: ['./modal-confirmation.component.css']
})
export class ModalConfirmationComponent implements OnInit {
  @Input() title!: string;
  @Input() message!: string;

  // if I called the service outside of a UI event (click, etc), then the DialogComponent would actually not initialize.
  // This is why you’ll see the injection of ChangeDetectorRef and that the call to “markForCheck()” has to be made. With this in place, the DialogService can be called as needed and work properly.

  constructor(public activeModal: NgbActiveModal, public changeRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    console.log("ngOnInit() ModalConfirmationComponent");
  }

  public decline() {
    this.activeModal.dismiss();
  }

  public accept() {
    this.activeModal.close();
  }

}
