import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from '../../services/modal.service';

import { ModalConfirmationComponent } from './modal-confirmation.component';

describe('ModalConfirmationComponent', () => {
  let component: ModalConfirmationComponent;
  let activeModal : NgbActiveModal;

  beforeEach(async () => {
    await TestBed.compileComponents().then(() => {
      let modalService : NgbModal = TestBed.get(NgbModal);
      const modalRef : NgbModalRef = modalService.open(ModalConfirmationComponent);
      component = modalRef.componentInstance;
      activeModal = component.activeModal;
    })
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('Call onDismiss()', () => {
    spyOn(activeModal, 'dismiss');
    component.decline();
    expect(activeModal.dismiss).toHaveBeenCalled();
  });
});
