import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { ModalAlertComponent } from './modal-alert.component';

describe('ModalAlertComponent', () => {
  let component: ModalAlertComponent;
  let activeModal : NgbActiveModal;

  beforeEach(async () => {
    await TestBed.compileComponents().then(() => {
      let modalService : NgbModal = TestBed.get(NgbModal);
      const modalRef : NgbModalRef = modalService.open(ModalAlertComponent);
      component = modalRef.componentInstance;
      activeModal = component.activeModal;
    })
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });

  it('Call onClose()', () => {
    spyOn(activeModal, 'close');
    component.onClose();
    expect(activeModal.close).toHaveBeenCalled();
  });
});
