import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-alert',
  templateUrl: './modal-alert.component.html',
  styleUrls: ['./modal-alert.component.css']
})
export class ModalAlertComponent implements OnInit {
  @Input() title!: string;
  @Input() message!: string;
  @Input() isError!: boolean;

  constructor(public activeModal: NgbActiveModal, public changeRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    console.log("ngOnInit() ModalAlertComponent");
  }

  onClose(): void {
    this.activeModal.dismiss();
  }
}
