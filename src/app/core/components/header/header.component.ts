import { Component, Input, OnInit } from '@angular/core';
import { AlertOptionsI } from '../../interfaces/alert-options-i';
import { ConfirmOptionsI } from '../../interfaces/confirm-options-i';
import { NavItemI } from '../../interfaces/nav-item-i';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title!: string;
  public navItems!: NavItemI[];

  constructor(private modalService: ModalService) { }

  ngOnInit(): void {
    this.navItems = [
      {
        label: 'Home',
        route: '/home'
      },
      {
        label: 'Settings',
        route: '/settings'
      },
      {
        label: 'Test',
        route: '/test'
      },
      {
        label: 'Angular Doc',
        href: 'https://www.angular.io'
      }
    ];
  }

  openModalInfo(): void {
    this.modalService.info(<AlertOptionsI>{title: "Custom Titre INFO", message: "Custom message INFO"}).subscribe(() => console.log("info fermée"));
  }

  openModalError(): void {
    this.modalService.error(<AlertOptionsI>{title: "Custom Titre ERROR", message: "Custom message ERROR"});
  }

  openModalConfirmError(): void {
    this.modalService
      .confirm(<ConfirmOptionsI>{title: "Mon titre", message: "Mon message", errorOnClose: true})
      .subscribe({
        next: () => console.log("Validé !"),
        error: () => console.log("error")
      });
  }

  openModalConfirm(): void {
    this.modalService
      .confirm(<ConfirmOptionsI>{title: "Mon titre", message: "Mon message"})
      .subscribe({
        next: () => console.log("Validé !")
      });
  }
}
